#!/bin/sh

set -eu

            echo "
            =======================================================================
            USER INFO:

            UID: $PUID
            GID: $PGID

            MORE INFO:

            If you have permission problems remember to use same user UID and GID.
            Check it with "id" command
            =======================================================================
            "

groupmod -o -g "$PGID" daboys-server  > /dev/null 2>&1
usermod -o -u "$PUID" daboys-server  > /dev/null 2>&1

chown -R daboys-server:daboys-server /home/daboys-server

su-exec daboys-server "$@"
